json.extract! user, :id, :name, :surname, :dni, :age, :address, :phone, :mobile, :symptom, :nationality, :family_group, :country_from, :residence_time, :disease, :patient_type, :created_at, :updated_at
json.url user_url(user, format: :json)
