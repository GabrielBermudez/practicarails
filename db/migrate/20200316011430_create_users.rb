class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :dni
      t.string :age
      t.string :address
      t.string :phone
      t.string :mobile
      t.string :symptom
      t.string :nationality
      t.string :family_group
      t.string :country_from
      t.string :residence_time
      t.string :disease
      t.string :patient_type

      t.timestamps
    end
  end
end
